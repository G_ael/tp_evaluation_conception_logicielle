# TP Evaluation et Conception de logiciels

Bienvenue dans notre application :)

## Démarrage rapide

Pour lancer rapidement l'application veuillez créer deux terminaux :
- Sur le premier, lancez `cd api/`pour vous rendre dans le dossier de l'api. 
Puis lancez votre webservice en local en suivant les instructions du README de ce dossier.
- Une fois la FastAPI mise en route, lancez `cd client/`dans le second terminal pour vous rendre dans le dossier client.
Vous pouvez alors lancer l'application en suivant les instructions du README de ce dossier.

## Objectif de l'application

L'objectif est double. D'une part, réaliser un webservice pouvant récupérer un deck de 52 cartes et tirer des cartes au sein de ce dernier. D'autre part, mettre en place un client de notre webservice capable de requéter cette API. Nous élaborons alors le scénario suivant pour ce client : Il initialise un deck, tire x cartes et compte le nombre de cartes tirées de chaque couleur.

## Organisation et fonctionnement du code

Cette application s'organise en deux dossiers :
- l'api
- la partie client

### L'api

L'api consiste en fait en la création d'une FastAPI.
Notre Webservice est lui même un client web pour l'api https://deckofcardsapi.com/. Notre api consiste alors à requêter cette api pour mener à bien l'élaboration de notre web service.

Notre webservice comporte 3 endpoints : 
- Le premier exposé en GET est un simple affichage à la racine ;
- Le second exposé en GET récupère un deck de l'api et renvoie son id ;
- Le troisième exposé en POST tire des cartes du deck.

### La partie client

La partie client consiste à mettre en place un client pour notre webservice capable de requêter ce dernier.
Elle permet ainsi d'initialiser un deck, de tirer des cartes de ce deck et possède également un fonctionnalité de comptage du nombre de cartes de chaque couleur dans un jeu de cartes.


                      ______________________________________
                     /   _______________________________    \
                    |   |            __ __              |   |
                    | _ |    /\     ) _) /''''',        |   |
                    |(.)|   <  >    \ ) // '  `,        |   |
                    | ` |    \/       \/// ~ |~:    +   |   |
                    |   |             ///*\  ' :    |   |   |
                    |   |            ///***\_~.'    |   |   |
                    |   |  .'.    __///````,,..\_   |   |   |
                    |   |   ` \ _/* +_\#\\~\ooo/ \  |   |   |
                    |   |     |/:\ + *\_\#\~\so/!!\ |   |   |
                    |   |    _\_::\ * + \-\#\\o/!!!\|   |   |
                    |   |   / <_=::\ + */_____@_!!!_|   |   |
                    |   |  <__/_____\ */( /\______ _|   |   |
                    |   |   |_   _ __\/_)/* \   ._/  >  |   |
                    |   |   | !!! @     /* + \::=_>_/   |   |
                    |   |   |\!!!/o\\#\_\ + * \::_\     |   |
                    |   |   | \!!/os\~\#_\_* + \:/|     |   |
                    |   |   |  \_/ooo\~\\#_\+_*/- \     |   |
                    |   |   |    \''``,,,,///      .`.  |   |
                    |   |   |     ,.- ***///        '   |   |
                    |   |   |    : ~  \*///             |   |
                    |   |   +    : |   \//\             |   |
                    |   |        ,~  ~ //_( \     /\    | ; |
                    |   |        ,'  ` /_(__(    <  >   |(_)|
                    |   |         `````           \/    |   |
                    |   |_______________________________|   |
                     \______________________________________/