from typing import Optional

from fastapi import FastAPI

import requests

app = FastAPI()


@app.get("/")
def read_root():
    """fct à la racine de notre webservice : simple affichage"""
    return {"Hello": "World"}


@app.get("/creer-un-deck/")
def get_id():
    """Fct qui récupère un deck de l'api et renvoie son id"""
    requete = requests.get("https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=1")
    deck_id = requete.json()["deck_id"]
    return deck_id


@app.post("/cartes/{nombre_cartes}")
def post_cartes(deck_id:str, nombre_cartes):
    """Fct qui tire des cartes du deck"""
    # Entrez new pour le deck_id si vous n'avez pas déjà crée un jeu de cartes.
    # Nous faisons ici le choix que dans le cas où le deck de l'utilisateur n'a plus de cartes, nous lui en créons un nouveau.
    # Il pourra observer cela dans le dictionnaire en sortie où il trouvera également son nouvel id.
    change = 0
    requeteVerif = requests.get("https://deckofcardsapi.com/api/deck/"+ deck_id)
    if requeteVerif.json()["remaining"] == 0:
        deck_id = "new"
        change = 1
    requete = requests.get("https://deckofcardsapi.com/api/deck/"+ deck_id + "/draw/?count=" + nombre_cartes)
    res = requete.json()
    if change == 1 :
        res["new"] = "Votre ancien deck était vide. Les cartes ont donc été tirées dans un nouveau jeu."
    return res
