# Notre Webservice

Cette partie concerne la mise en place d'une fastAPI. 
Ci-dessous le code à exécuter pour lancer l'API.

# installer les dépendances
```
pip3 install -r requirements.txt
```

# lancer la FastAPI
```
uvicorn main:app
```

Pour émettre des requêtes au service web que vous faites tourner en local, ouvrez la page internet suivante : http://127.0.0.1:8000/docs