# Client de notre webservice

Cette partie concerne les requêtes du client sur l'API. 
Ci-dessous, le code pour lancer l'application.

## Installer les dépendances 

```
pip3 install -r requirements.txt
```

## Lancer l'application

```
python3 main.py
```

## Tests unitaires

Les tests unitaires s'exécutent automatiquement grâce à la pipeline. Vous pouvez néanmoins vous assurer qu'ils fonctionnent avec la ligne de commande suivante :

```
pytest
```
