import pytest

from main import nbr_cartes_chaque_couleur

def test_nbr_carte():
    dic = {'H': 0, 'S': 0, 'D': 1, 'C': 1}
    cartes = [{'code': '9D', 'image': 'https://deckofcardsapi.com/static/img/9D.png', 
    'images': {'svg': 'https://deckofcardsapi.com/static/img/9D.svg', 'png': 'https://deckofcardsapi.com/static/img/9D.png'}, 
    'value': '9', 'suit': 'DIAMONDS'}, 
    {'code': 'JC', 'image': 'https://deckofcardsapi.com/static/img/JC.png', 
    'images': {'svg': 'https://deckofcardsapi.com/static/img/JC.svg', 
    'png': 'https://deckofcardsapi.com/static/img/JC.png'}, 'value': 'JACK', 'suit': 'CLUBS'}]
    dict_cartes = nbr_cartes_chaque_couleur(cartes)
    assert dict_cartes == dic
    