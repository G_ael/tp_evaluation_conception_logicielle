import requests


def nbr_cartes_chaque_couleur(cartes):
    """ Fonctionnalité permettant de calculer, pour une liste de cartes donnée en entrée,
     le nombre de cartes de chaque couleur sous forme d'un dictionnaire. """
    dic = {"H": 0, "S": 0, "D": 0, "C": 0}
    for i in range(len(cartes)) :
        if cartes[i]["suit"] == 'HEARTS' :
            dic["H"]+=1
        if cartes[i]["suit"] == 'SPADES' :
            dic["S"]+=1
        if cartes[i]["suit"] == 'DIAMONDS' :
            dic["D"]+=1
        if cartes[i]["suit"] == 'CLUBS' :
            dic["C"]+=1
    return dic



if __name__ == "__main__":
    """Lancement de l'application"""

    print("Bienvenue dans notre petit scénario ^^")
    
    #### Initialisation du deck
    r = requests.get("http://localhost:8000/creer-un-deck/")
    #print(r.status_code)
    id_deck = r.json()
    print("Nous vous avons initialisé un nouveau deck")
    
    #### Tirage des cartes du deck
    print("Combien de cartes souhaitez-vous tirer de votre deck (max 52) ?")
    nbr_cartes = input("> ")
    r = requests.post("http://localhost:8000/cartes/" + nbr_cartes + "?deck_id=" + id_deck)
    #print(r.status_code)
    cartes = r.json()["cards"]    

    print("Vos cartes viennent d'être tirées !")
    print("Voici le nombre de cartes de chaque couleur que vous possédez :")
    
    #### Fonctionnalité de comptage
    dict_cartes = nbr_cartes_chaque_couleur(cartes)
    print(dict_cartes)